package com.restService.conrollers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Conroller {

    @GetMapping("/service")
    public Service service() {
        return new Service("restservice");
    }
}

class Service {

    private String service;

    public Service(String service) {
        this.service = service;
    }

    public String getService() {
        return service;
    }
}


